.. raw:: html

    <style> .purple {color:purple} </style>
	
.. role:: purple

.. raw:: html

    <style> .white {color:white} </style>

.. role:: white

#############
Scope & Usage
#############

Scope
-----

Integration in the SMAP workflow
--------------------------------

.. image:: ../images/grm/SMAP_global_scheme_home_grm.png

**SMAP grm** is run on genotype call tables created by **SMAP haplotype-sites** and **SMAP haplotype-window**.  
**SMAP grm** works on GBS, HiPlex and Shotgun sequencing data.

Commands & options
------------------

Example commands
----------------

Output
------