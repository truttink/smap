.. raw:: html

    <style> .purple {color:purple} </style>
	
.. role:: purple

.. raw:: html

    <style> .white {color:white} </style>

.. role:: white

###################
Feature Description
###################

.. _SMAPsnpseqdef:

Definition of SNPs, regions, off-set, and distances
---------------------------------------------------


