.. SMAP documentation master file, created by
   sphinx-quickstart on Wed Aug  5 13:28:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SMAP's documentation!
================================

.. subprojecttoctree::
   :maxdepth: 2
   :caption: Contents:
   
   Home <home>
   Installation & Quick Start <quickstart/index>
   SMAP delineate <delineate/index>
   SMAP sliding-frames <sliding_frame/index>
   SMAP compare <compare/index>
   SMAP haplotype-sites <sites/index>
   SMAP snp-seq <snp-seq/index>
   SMAP target-selection <target-selection/index>
   SMAP design <design/index>
   SMAP haplotype-window <window/index>
   SMAP effect-prediction <effect/index>
   SMAP grm <grm/index>