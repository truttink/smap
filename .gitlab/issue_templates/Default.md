## Version check
I have:
- [ ] checked that this issue has not been reported already
- [ ] checked that I am using the latest version of SMAP

## Installation information
### Output from `pip --list`:

### Output from `python --version`:

### I have SMAP installed using:
- [ ] pip
- [ ] docker

## Steps to reproduce the issue
Please provide at least: 
 - (1) a small sample data set that can be used to reduce the problem. This can be done with a file upload (max. 10MB), or (if the example is small enough) by pasting it here 
 - (2) The command line arguments that were used.

## What is the bug behavior?
Describe the actual problem. What was the output from SMAP?

## What is the correct behavior?
Describe what should actually happen.

## (Optional) Other relevant logs or screenshots

/label ~bug